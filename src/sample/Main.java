package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main extends Application {
    public static final String BUILD_NUMBER = "1";
    public static final String BUILD_DATE = "20171213-120621";

    @Override
    public void start(Stage primaryStage) throws Exception{

        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd-HHmmss");
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd. MM. yyyy,  HH:mm:ss");
        Date date = null;
        try {
            date = format.parse(BUILD_DATE);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outDateVersion = outputFormat.format(date);
        Parent root = FXMLLoader.load(this.getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Zapsi Alarm Server 1.5." + BUILD_NUMBER + "      (" + outDateVersion + ")");
        primaryStage.setScene(new Scene(root, 1024.0, 768.0));
        primaryStage.setResizable(false);
        primaryStage.getIcons().add(new Image(sample.Main.class.getResourceAsStream("icon.png")));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
