
package sample;

import com.mysql.jdbc.Statement;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.util.Duration;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.input.ReversedLinesFileReader;

import java.io.*;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import static java.nio.charset.StandardCharsets.UTF_8;

public class Controller
        implements Initializable {
    public Button installServiceButton;
    public Button runServiceButton;
    public Button saveButton;
    public Text serviceInstallationStatus;
    public Text serviceRunningStatus;
    public TextField databaseUsername;
    public TextField databasePassword;
    public TextField databaseAddress;
    public TextField databaseName;
    public TextField customer;
    public TextField emailsFrom;
    public TextField emailsTo;
    public TextField smtpHost;
    public TextField smtpPassword;
    public TextField port;
    public TextArea logArea;
    public TextArea alarmsFromSQL;
    public TextArea alarmsHistoryFromSQL;
    public CheckBox pauseButton;
    public TextField filterField;
    public CheckBox SSL;
    private boolean serviceInstalled = false;
    private boolean serviceRunning = false;
    private boolean pauseLogCheckBoxIsSelected = false;
    private static Connection mySQLConnection;
    private ServiceStatus serviceStatus = ServiceStatus.NONE;

    public enum ServiceStatus {
        RUNNING,
        STOPPED,
        NONE
    }

    public void initialize(URL location, ResourceBundle resources) {
        System.out.println("Starting");
        try {
            createConfigFile();
            loadSettingsFromFile();
            initiateLogTab();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        checkForServiceStatus();
    }


    private void initiateLogTab() throws InterruptedException {
        logArea.clear();
        refreshLogAndInfoStatus();
        KeyFrame[] arrkeyFrame = new KeyFrame[1];
        arrkeyFrame[0] = new KeyFrame(Duration.seconds(1.0), event -> refreshLogAndInfoStatus());
        Timeline logRefresh = new Timeline(arrkeyFrame);
        logRefresh.setCycleCount(-1);
        logRefresh.play();
    }

    private void refreshLogAndInfoStatus() {

        try {
            String dataToDisplayInLogTextField = "";
            FileReader fr = new FileReader("log\\log.txt");
            BufferedReader br = new BufferedReader(fr);
            if (!pauseLogCheckBoxIsSelected) {
                updateLogViewFromFile(dataToDisplayInLogTextField);
            }
            fr.close();
            br.close();
        } catch (IOException e) {
            System.out.println("No log file present");
        }
    }

    private void updateLogViewFromFile(String dataToDisplayInLogTextField) throws IOException {
        try {
            File file = new File("log\\log.txt");
            int linesToDisplay = 100;
            ReversedLinesFileReader object = new ReversedLinesFileReader(file, UTF_8);
            StringBuilder dataToDisplayInLogTextFieldBuilder = new StringBuilder(dataToDisplayInLogTextField);
            if (file.length() - 1 < 100) {
                linesToDisplay = (int) (file.length() - 1);
            }
            for (int i = 0; i < linesToDisplay; i++) {
                String line = object.readLine();
                if (line == null) {
                    break;
                }
                if (filterField.getText().length() > 0) {
                    if (line.contains(filterField.getText())) {
                        dataToDisplayInLogTextFieldBuilder.append(line).append("\n");
                    }
                } else {
                    dataToDisplayInLogTextFieldBuilder.append(line).append("\n");
                }
            }
            dataToDisplayInLogTextField = dataToDisplayInLogTextFieldBuilder.toString();
            logArea.setText(dataToDisplayInLogTextField);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void loadSettingsFromFile() throws IOException {
        String actualLine;
        FileReader fr = new FileReader("config.txt");
        BufferedReader br = new BufferedReader(fr);
        while ((actualLine = br.readLine()) != null) {
            if (actualLine.contains("[databaseAddress]")) {
                databaseAddress.setText(actualLine.substring(18));
            } else if (actualLine.contains("[databaseName]")) {
                databaseName.setText(actualLine.substring(15));
            } else if (actualLine.contains("[userName]")) {
                databaseUsername.setText(actualLine.substring(11));
            } else if (actualLine.contains("[userPassword]")) {
                databasePassword.setText(actualLine.substring(15));
            } else if (actualLine.contains("[customer]")) {
                customer.setText(actualLine.substring(11));
            } else if (actualLine.contains("[emailsFrom]")) {
                emailsFrom.setText(actualLine.substring(13));
            } else if (actualLine.contains("[port]")) {
                port.setText(actualLine.substring(7));
            } else if (actualLine.contains("[emailTo]")) {
                emailsTo.setText(actualLine.substring(10));
            } else if (actualLine.contains("[smtpHost]")) {
                smtpHost.setText(actualLine.substring(11));
            } else if (actualLine.contains("[smtpPassword]")) {
                smtpPassword.setText(actualLine.substring(15));
            } else if (actualLine.contains("[SSL]")) {
                if (actualLine.contains("true")) {
                    SSL.setSelected(true);
                } else {
                    SSL.setSelected(false);
                }
            }
        }
        br.close();
        fr.close();
    }

    private void checkForServiceStatus() {
        try {
            String line;
            Process process = new ProcessBuilder("sc", "query", "AlarmServerService").start();
            InputStream is = process.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder scOutput = new StringBuilder();
            while ((line = br.readLine()) != null) {
                scOutput.append(line).append("\n");
            }
            System.out.println(scOutput);
            updateServiceStatus(scOutput);
        } catch (IOException e) {
            System.out.println("No service present");
        }

    }

    private void updateServiceStatus(StringBuilder scOutput) {

        if (scOutput.toString().contains("RUNNING")) {
            serviceStatus = ServiceStatus.RUNNING;
            setServiceRunning();
        } else if (scOutput.toString().contains("STOPPED")) {
            serviceStatus = ServiceStatus.STOPPED;
            setServiceInstalled();
        } else if (scOutput.toString().contains("START_PENDING")) {
            serviceStatus = ServiceStatus.RUNNING;
            setServiceRunning();
        } else {
            serviceStatus = ServiceStatus.NONE;
            setServiceUninstalled();
        }
    }

    private void setServiceUninstalled() {
        serviceRunningStatus.setText("Service stopped");
        serviceRunningStatus.setStyle("-fx-fill: red");
        serviceInstallationStatus.setText("Service not installed");
        serviceInstallationStatus.setStyle("-fx-fill: red");
        installServiceButton.setText("Install");
        installServiceButton.setDisable(false);
        runServiceButton.setText("-");
        runServiceButton.setDisable(true);
        serviceInstalled = false;
        serviceRunning = false;
    }

    private void setServiceInstalled() {
        serviceRunningStatus.setText("Service stopped");
        serviceRunningStatus.setStyle("-fx-fill: red");
        serviceInstallationStatus.setText("Service installed");
        serviceInstallationStatus.setStyle("-fx-fill: green");
        installServiceButton.setText("Uninstall");
        installServiceButton.setDisable(false);
        runServiceButton.setText("Start");
        runServiceButton.setDisable(false);
        serviceInstalled = true;
        serviceRunning = false;
    }

    private void setServiceRunning() {
        serviceRunningStatus.setText("Service running");
        serviceRunningStatus.setStyle("-fx-fill: green");
        serviceInstallationStatus.setText("Service installed");
        serviceInstallationStatus.setStyle("-fx-fill: green");
        installServiceButton.setText("---");
        installServiceButton.setDisable(true);
        runServiceButton.setText("Stop");
        runServiceButton.setDisable(false);
        serviceInstalled = true;
        serviceRunning = true;
    }

    private void createConfigFile() throws IOException {
        File file = new File("config.txt");
        if (!file.exists()) {
            System.out.println("Config file created: " + file.createNewFile());
            String content = "" +
                    "[databaseAddress] localhost\n" +
                    "[databaseName] zapsi2\n" +
                    "[userName] zapsi_uzivatel\n" +
                    "[userPassword] zapsi\n" +
                    "[deleteInterval] 10\n" +
                    "[customer] zapsi\n" +
                    "[emailsFrom] support@zapsi.eu\n" +
                    "[port] 465\n[emailTo] zapsi@zapsi.eu\n" +
                    "[smtpHost] smtp.forpsi.com\n" +
                    "[smtpPassword] support01..\n" +
                    "[SSL] true";
            FileUtils.writeStringToFile(file, content, UTF_8);
        }
    }

    public void changeServiceInstallStatus() {
        if (!serviceInstalled) {
            installService();
        } else if (!serviceRunning) {
            uninstallService();
        }
    }

    private void uninstallService() {
        try {
            while (serviceStatus.equals(ServiceStatus.STOPPED)) {
                Runtime.getRuntime().exec("sc delete AlarmServerService");
                checkForServiceStatus();
            }
            setServiceUninstalled();

        } catch (IOException e) {
            System.out.println("Missing service file.");
        }
    }

    private void installService() {
        try {
            File service = new File("AlarmServerService.exe");
            String command = "sc create AlarmServerService binPath= \"" + service.getAbsolutePath() + "\" start= auto";
            while (serviceStatus.equals(ServiceStatus.NONE)) {
                Runtime.getRuntime().exec(command);
                checkForServiceStatus();
            }
            setServiceInstalled();
        } catch (IOException e) {
            System.out.println("Missing service file.");
        }
    }

    public void changeServiceRunStatus() throws InterruptedException, IOException {
        if (serviceRunning) {
            while (serviceStatus.equals(ServiceStatus.RUNNING)) {
                stopService();
                checkForServiceStatus();
            }
            setServiceInstalled();
        } else if (serviceInstalled) {
            while (serviceStatus.equals(ServiceStatus.STOPPED)) {
                runService();
                checkForServiceStatus();
            }
            setServiceRunning();
        }
        Thread.sleep(3000);
        checkForServiceStatus();
    }

    private void runService() throws IOException {
        Runtime rt = Runtime.getRuntime();
        rt.exec("sc start AlarmServerService");
    }

    private void stopService() throws IOException {
        Runtime rt = Runtime.getRuntime();
        rt.exec("sc stop AlarmServerService");
    }

    public void saveConfig() throws IOException {
        saveDataToFile();
    }

    private void saveDataToFile() throws IOException {
        File file = new File("config.txt");
        System.out.println("File created: " + file.createNewFile());
        FileWriter fw = new FileWriter(file.getAbsoluteFile(), false);
        BufferedWriter bw = new BufferedWriter(fw);
        String content = "" +
                "[databaseAddress] " + databaseAddress.getText() +
                "\n[databaseName] " + databaseName.getText() +
                "\n[userName] " + databaseUsername.getText() +
                "\n[userPassword] " + databasePassword.getText() +
                "\n[deleteInterval] 10\n[customer] " + customer.getText() +
                "\n[emailsFrom] " + emailsFrom.getText() +
                "\n[port] " + port.getText() +
                "\n[emailTo] " + emailsTo.getText() +
                "\n[smtpHost] " + smtpHost.getText() +
                "\n[smtpPassword] " + smtpPassword.getText() +
                "\n[SSL] " + SSL.isSelected();
        bw.write(content);
        bw.close();
        System.out.println("saved");
    }


    public void changePauseSettings() {
        pauseLogCheckBoxIsSelected = pauseButton.isSelected();
    }

    public void alarmsTabSelected() throws SQLException {
        boolean databaseConnected = connectToDatabase();
        if (databaseConnected) {
            updateAlarmsFromSQL();
            updateAlarmsHistory();
        }
        mySQLConnection.close();
    }

    private void updateAlarmsHistory() throws SQLException {
        connectToDatabase();
        String query = "SELECT * FROM zapsi2.alarm";
        StringBuilder alarmsForGUI = new StringBuilder();
        try {
            Statement statement = (Statement) mySQLConnection.createStatement();
            ResultSet actualLine = statement.executeQuery(query);
            while (actualLine.next()) {
                alarmsForGUI.append(actualLine.getString("Name")).append(" -> ");
                alarmsForGUI.append(actualLine.getString("Email")).append("\n");
            }
            alarmsFromSQL.setText(alarmsForGUI.toString());
        } catch (Exception e) {
            System.out.println("Cannot load alarms history");
        }
    }

    private void updateAlarmsFromSQL() throws SQLException {
        connectToDatabase();
        String query = "SELECT DTS, DTE, Name FROM zapsi2.alarm_history INNER JOIN zapsi2.alarm WHERE zapsi2.alarm.OID = zapsi2.alarm_history.alarmID ORDER BY DTS DESC LIMIT 50";
        StringBuilder alarmsHistoryForGUI = new StringBuilder();
        try {
            Statement statement = (Statement) mySQLConnection.createStatement();
            ResultSet actualLine = statement.executeQuery(query);
            while (actualLine.next()) {
                alarmsHistoryForGUI.append(actualLine.getString("DTS")).append(" -> ");
                alarmsHistoryForGUI.append(actualLine.getString("DTE")).append(" -> ");
                alarmsHistoryForGUI.append(actualLine.getString("Name")).append("\n");
            }
            alarmsHistoryFromSQL.setText(alarmsHistoryForGUI.toString());
        } catch (Exception e) {
            System.out.println("Cannot load alarms from SQL");
        }
    }

    private boolean connectToDatabase() {
        boolean databaseConnected = false;
        try {
            String databaseConnection = "jdbc:mysql://" + databaseAddress.getText() + ":3306/" + databaseName.getText() + "?autoReconnect=true&useSSL=false";
            mySQLConnection = DriverManager.getConnection(databaseConnection, databaseUsername.getText(), databasePassword.getText());
            if (mySQLConnection != null) {
                System.out.println("Database connected");
            }
            databaseConnected = true;
        } catch (SQLException cannotConnect) {
            System.out.println("Database connect error");
            alarmsHistoryFromSQL.setText("Not possible to connect to the database.");
            alarmsFromSQL.setText("Not possible to connect the database.");
        }
        return databaseConnected;
    }
}

