# Zapsi Alarm Server
GUI that manages service that sends alarm emails from system made by www.zapsi.eu.
Service is here https://github.com/jahaman/ZapsiAlarmServerService

## Features
* show "what is happening" log
* show alarm templates
* show alarms history
* settings
  * database connection
  * email settings
* filtering

www.zapsi.eu © 2017

Download: [AlarmServer1.5.zip](https://github.com/jahaman/ZapsiAlarmServerGUI-JavaFX/files/1555267/AlarmServer1.5.zip)

![image 001](https://user-images.githubusercontent.com/11396401/32142064-fd7721d4-bc8f-11e7-8657-b77c93cd98e6.png)


